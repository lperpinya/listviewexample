package com.example.listviewexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.listviewexample.logic.SistemaOperatiu;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ArrayList<SistemaOperatiu> llistaSistemes;
    private String[] llistaString = {"Windows", "Ubuntu", "Android", "Ios13"};
    private String[] llistaURL = {"http://www.microsoft.com", "http://www.ubuntu.com", "http://developer.android.com",
    "http://www.apple.com"};
    private int[] llistaInt = {R.drawable.guindows, R.drawable.ubuntu, R.drawable.android, R.drawable.ios};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        llistaSistemes = new ArrayList();
        for (int i = 0; i<llistaString.length; i++)
        {
            llistaSistemes.add(new SistemaOperatiu(llistaString[i], llistaInt[i], llistaURL[i]));
        }

        ListView lvLlista = findViewById (R.id.lvLlista);
        SistemaOperatiuAdapter adapter = new SistemaOperatiuAdapter(this, llistaSistemes);
        lvLlista.setAdapter(adapter);
        lvLlista.setOnItemClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent();
        intent.setAction ("android.intent.action.VIEW");
        intent.setData (Uri.parse(llistaSistemes.get(position).getUrl()));
        startActivity(intent);


    }
}
