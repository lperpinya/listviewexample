package com.example.listviewexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.listviewexample.logic.SistemaOperatiu;

import java.util.ArrayList;

public class SistemaOperatiuAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<SistemaOperatiu> llistaSistemes;

    public SistemaOperatiuAdapter(Context context, ArrayList<SistemaOperatiu> llistaSistemes) {
        this.context = context;
        this.llistaSistemes = llistaSistemes;
    }

    @Override
    public int getCount() {
        return llistaSistemes.size();
    }

    @Override
    public Object getItem(int position) {
        return llistaSistemes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_layout, parent, false);

        }
        TextView tvNombre = convertView.findViewById(R.id.tvNombre);
        tvNombre.setText(llistaSistemes.get(position).getNom());

        ImageView imgFoto = convertView.findViewById(R.id.imgFoto);
        imgFoto.setImageResource(llistaSistemes.get(position).getFoto());

        return convertView;

    }

}