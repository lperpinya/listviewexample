package com.example.listviewexample.logic;

public class SistemaOperatiu {

    private String nom;
    private int foto;
    private String Url;

    public String getUrl() {
        return Url;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public SistemaOperatiu(String nom, int foto, String Url) {
        this.nom = nom;
        this.foto = foto;
        this.Url = Url;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}
